
export class ExposedPort {

    constructor(private port: number, private url: string) {}

    getPort(): number {
        return this.port;
    }

    getUrl(): string {
        return this.url;
    }

}
