import { HttpClient } from './http-client';


export class Recaptcha {

    private resolve: Function;
    private reject: Function;
    private domNode: any;

    constructor() {}

    display(domNode: any): Promise<string> {
        this.domNode = domNode;

        const newScriptTag = document.createElement('script');
        newScriptTag.type = 'text/javascript';
        newScriptTag.async = true;
        newScriptTag.src = 'https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit';
        const s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(newScriptTag, s);

        (<any>window).onloadCallback = this.onLoad.bind(this);

        return new Promise((resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
        });
    }

    onLoad() {
        (<any>window).grecaptcha.render(this.domNode, {'sitekey': '6Ld8pREUAAAAAOkrGItiEeczO9Tfi99sIHoMvFA_', 'callback': this.handleResponse.bind(this)});
    }

    handleResponse(captchaResponse: string) {
        this.resolve(captchaResponse);
    }

}
