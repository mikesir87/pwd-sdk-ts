const Terminal = require('xterm');
import { Instance, InstanceDetails } from './instance';

/**
 * Utility class that contains various code that's hard to include directly 
 * in classes because it's hard to test when inlined.
 * 
 * @author Michael Irwin
 */
export class Util {

    static newInstance(instanceOptions: InstanceDetails): Instance {
        return new Instance(instanceOptions);
    }

    static newSocketIoConnection(hostname: string, options: any): SocketIOClient.Socket {
        return io(hostname, options);
    }

    static newTerminal(): any {
        return new Terminal({ cursorBlink: false });
    }

    static querySelectorAll(selector: string): NodeListOf<Element> {
        return document.querySelectorAll(selector);
    }

    static querySelector(selector: string): Element | null {
        return document.querySelector(selector);
    }

}