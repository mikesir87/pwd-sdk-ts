import { Instance, InstanceOptions } from './instance';
import { Util } from './util';

describe('Instance', () => {
    const ip = '192.168.12.2';
    const name = 'node_0';
    const url = 'http://some-url-for-the-ip-and-name';

    let exposedPortUrlGenerator: jasmine.Spy;
    let instanceOptions : InstanceOptions = { terminalSelector: "#term1" };
    let instance: Instance;

    let originalNewTerminal = Util.newTerminal;
    let overriddenNewTerminal: jasmine.Spy;

    let originalQuerySelectorAll = Util.querySelectorAll;
    let overriddenQuerySelectorAll: jasmine.Spy;

    const elements = [{id: 1}, {id: 2}];
    const geometry = { cols: 60, rows: 15 };
    let terminal: { 
        open: jasmine.Spy, on: jasmine.Spy, 
        attachCustomKeydownHandler: jasmine.Spy, 
        proposeGeometry: jasmine.Spy, write: jasmine.Spy,
        destroy: jasmine.Spy,
        resize: jasmine.Spy,
    };

    let capturedKeyboardHandler: Function;
    let capturedDataHandler: Function;

    beforeEach(() => {
        exposedPortUrlGenerator = jasmine.createSpy('exposedPortUrlGenerator').and.returnValue(url);
        instance = new Instance({ ip, name, exposedPortUrlGenerator, options: instanceOptions });

        terminal = {
            open: jasmine.createSpy('terminal.open'),
            on: jasmine.createSpy('terminal.on').and.callFake((event: string, handler: Function) => { expect(event).toBe('data'); capturedDataHandler = handler; }),
            attachCustomKeydownHandler: jasmine.createSpy('terminal.attachCustomKeydownHandler').and.callFake((handler: Function) => capturedKeyboardHandler = handler),
            proposeGeometry: jasmine.createSpy('terminal.proposeGeometry').and.returnValue(geometry),
            write: jasmine.createSpy('terminal.write'),
            destroy: jasmine.createSpy('terminal.destroy'),
            resize: jasmine.createSpy('terminal.resize'),
        };

        overriddenNewTerminal = jasmine.createSpy('newTerminal').and.returnValue(terminal);
        Util.newTerminal = overriddenNewTerminal;

        overriddenQuerySelectorAll = jasmine.createSpy('querySelectorAll').and.returnValue(elements);
        Util.querySelectorAll = overriddenQuerySelectorAll;
    });

    afterEach(() => {
        instance.destroy();
        Util.newTerminal = originalNewTerminal;
        Util.querySelectorAll = originalQuerySelectorAll;
    })

    it('constructs the instance correctly', () => {
        expect(instance.getName()).toBe(name);
        expect(instance.getIp()).toBe(ip);
    });

    it('sets and emits events on cpu events', () => {
        const metricsSpy = jasmine.createSpy('cpuMetrics');
        instance.on('metrics.cpu', metricsSpy);
        instance.setCpuMetrics('metric-1');

        expect(metricsSpy).toHaveBeenCalledWith('metric-1');
        expect(instance.getCpuMetrics()).toBe('metric-1');
    });

    it('does not emit events on same cpu metric values', () => {
        const metricsSpy = jasmine.createSpy('cpuMetrics');
        instance.setCpuMetrics('metric-1');
        
        instance.on('metrics.cpu', metricsSpy);
        instance.setCpuMetrics('metric-1');

        expect(metricsSpy).not.toHaveBeenCalled();
        expect(instance.getCpuMetrics()).toBe('metric-1');
    });

    it('sets and emits events on memory events', () => {
        const metricsSpy = jasmine.createSpy('memoryMetrics');
        instance.on('metrics.memory', metricsSpy);
        instance.setMemoryMetrics('metric-1');

        expect(metricsSpy).toHaveBeenCalledWith('metric-1');
        expect(instance.getMemoryMetrics()).toBe('metric-1');
    });

    it('does not emit events on same memory metric values', () => {
        const metricsSpy = jasmine.createSpy('memoryMetrics');
        instance.setMemoryMetrics('metric-1');
        
        instance.on('metrics.memory', metricsSpy);
        instance.setMemoryMetrics('metric-1');

        expect(metricsSpy).not.toHaveBeenCalled();
        expect(instance.getMemoryMetrics()).toBe('metric-1');
    });

    it('adds ports and emits events', () => {
        const portAddSpy = jasmine.createSpy('portAddSpy');
        instance.on('port.new', portAddSpy);
        instance.setExposedPorts([80]);

        expect(portAddSpy).toHaveBeenCalledWith(jasmine.objectContaining({
            port: 80, url
        }));

        const exposedPorts = instance.getExposedPorts();
        expect(exposedPorts.length).toBe(1);
        expect(exposedPorts[0].getPort()).toBe(80);
        expect(exposedPorts[0].getUrl()).toBe(url);
    });

    it('removes ports when no longer exposed', () => {
        const portRemovalSpy = jasmine.createSpy('portRemoveSpy');
        instance.on('port.removed', portRemovalSpy);

        instance.setExposedPorts([80]);
        expect(portRemovalSpy).not.toHaveBeenCalled();

        instance.setExposedPorts([]);
        expect(portRemovalSpy).toHaveBeenCalledWith(jasmine.objectContaining({
            port: 80, url
        }));
        expect(instance.getExposedPorts().length).toBe(0);
    });

    it('handles additional port exposed', () => {
        instance.setExposedPorts([80]);
        const portAddSpy = jasmine.createSpy('portAddSpy');
        instance.on('port.new', portAddSpy);
        
        instance.setExposedPorts([80, 443]);
        expect(portAddSpy).toHaveBeenCalledWith(jasmine.objectContaining({
            port: 443, url
        }));
        
        expect(instance.getExposedPorts().length).toBe(2);
    });

    it('removes event handlers when unregistering', () => {
        const spy = jasmine.createSpy('spy');
        instance.on('metrics.cpu', spy);
        instance.off('metrics.cpu', spy);

        instance.setCpuMetrics('some-metrics');

        expect(spy).not.toHaveBeenCalled();
    });

    it('configures the terminal correctly', () => {
        instance = new Instance({ ip, name, exposedPortUrlGenerator, options: { terminalSelector: '.term' } });

        expect(overriddenQuerySelectorAll).toHaveBeenCalledWith('.term')
        expect(overriddenNewTerminal).toHaveBeenCalled();
        expect(terminal.open).toHaveBeenCalledWith(elements[0], { focus: true });
        expect(terminal.on).toHaveBeenCalled();
    });

    it('emits terminal data through event handler', () => {
        instance = new Instance({ ip, name, exposedPortUrlGenerator, options: { terminalSelector: '.term' } });

        // Validate data handler emits 'terminal.data'
        let dataEventListener = jasmine.createSpy('terminal.data-eventHandler');
        instance.on('terminal.data', dataEventListener);

        capturedDataHandler("DATA");
        expect(dataEventListener).toHaveBeenCalledWith("DATA");
    });

    it('emits terminal keydown handlers', () => {
        instance = new Instance({ ip, name, exposedPortUrlGenerator, options: { terminalSelector: '.term' } });

        // Validate terminal keydown handler
        let keydownHandler = jasmine.createSpy('terminal.keydown-eventHandler');
        instance.on('terminal.keydown', keydownHandler);
        const event = {};
        capturedKeyboardHandler(event);
        expect(keydownHandler).toHaveBeenCalledWith(terminal, event);
    });

    it('sends writes through to the terminal', () => {
        instance = new Instance({ ip, name, exposedPortUrlGenerator, options: { terminalSelector: '.term' } });

        const data = "SOME DATA";
        instance.write(data);
        expect(terminal.write).toHaveBeenCalledWith(data);
    });

    it('sends resizes through to the terminal', () => {
        instance = new Instance({ ip, name, exposedPortUrlGenerator, options: { terminalSelector: '.term' } });

        instance.resize(10, 20);
        expect(terminal.resize).toHaveBeenCalledWith(10, 20);
    });

});
