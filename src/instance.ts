const Terminal = require('xterm');
require('xterm/lib/addons/fit/fit.js');
// require('xterm/dist/xterm.css');

import Emitter = require('tiny-emitter');
import { ExposedPort } from './exposed-port';
import { Util } from './util';

export interface InstanceOptions {
    terminalSelector: string;
}

export interface InstanceDetails {
    name: string;
    ip: string;
    options: InstanceOptions;
    exposedPortUrlGenerator: (instance: Instance, port: number) => string;
}


export type InstanceEvent = 'metrics.cpu' | 'metrics.memory' | 'port.new' | 'port.removed' |
    'terminal.data' | 'terminal.keydown' | 'terminal.viewport.resized';

export const InstanceEvents: InstanceEvent[] = [
    'metrics.cpu', 'metrics.memory', 'port.new', 'port.removed',
    'terminal.data', 'terminal.keydown', 'terminal.viewport.resized',
];


export class Instance {

    private name: string;
    private ip: string;
    private cpuMetrics: string;
    private memoryMetrics: string;
    private exposedPorts: ExposedPort[];

    private terminals: any[];
    private eventEmitter: Emitter;
    private exposedPortUrlGenerator: (instance: Instance, port: number) => string;

    constructor(instanceDetails: InstanceDetails) {
        this.name = instanceDetails.name;
        this.ip = instanceDetails.ip;
        this.eventEmitter = new Emitter();
        this.exposedPortUrlGenerator = instanceDetails.exposedPortUrlGenerator;

        this.terminals = [];
        this.exposedPorts = [];

        const elements = Util.querySelectorAll(instanceDetails.options.terminalSelector);
        for (let i = 0; i < elements.length; i++) {
            this.terminals.push(this.createTerminal(elements[i]));
        }
    }

    getName(): string {
        return this.name;
    }

    getIp(): string {
        return this.ip;
    }

    write(data: string) {
        this.terminals.forEach(term => term.write(data));
    }

    resize(cols: number, rows: number) {
        this.terminals.forEach(term => term.resize(cols, rows));
    }

    setCpuMetrics(metrics: string) {
        const changed = (metrics !== this.cpuMetrics);
        this.cpuMetrics = metrics;
        if (changed)
            this.emit('metrics.cpu', this.cpuMetrics);
    }

    getCpuMetrics() {
        return this.cpuMetrics;
    }

    setMemoryMetrics(metrics: string) {
        const changed = (metrics !== this.memoryMetrics);
        this.memoryMetrics = metrics;
        if (changed)
            this.emit('metrics.memory', this.memoryMetrics);
    }

    getMemoryMetrics() {
        return this.memoryMetrics;
    }

    setExposedPorts(ports: number[]) {
        ports
            .filter(port => this.exposedPorts.filter(p => p.getPort() === port).length === 0)
            .forEach(portNumber => {
                const newPort = new ExposedPort(portNumber, this.exposedPortUrlGenerator(this, portNumber));
                this.exposedPorts.push(newPort);
                this.emit('port.new', newPort);
            });

        // Remove ports that are no longer exposed
        this.exposedPorts = this.exposedPorts
            .filter(exposedPort => {
                const stillExposed = ports.filter(p => exposedPort.getPort() === p).length > 0;
                if (!stillExposed)
                    this.emit('port.removed', exposedPort);
                return stillExposed;
            });
    }

    getExposedPorts(): ExposedPort[] {
        return this.exposedPorts;
    }

    on(eventName: InstanceEvent, callback: Function) {
        this.eventEmitter.on(eventName, callback);
        return this;
    }

    off(eventName: InstanceEvent, callback: Function) {
        this.eventEmitter.off(eventName, callback);
        return this;
    }

    destroy() {
        this.terminals.forEach(term => term.destroy());
        InstanceEvents.forEach(eventType => this.eventEmitter.off(eventType));
    }

    private createTerminal(domNode: any) {
        const terminal = Util.newTerminal();

        terminal.open(domNode, { focus: true });
        terminal.on('data', (data: string) => {
            this.emit('terminal.data', data);
        });

        terminal.attachCustomKeydownHandler((event: any) => {
            this.emit('terminal.keydown', terminal, event);
        });

        const size = terminal.proposeGeometry();
        this.emit('terminal.viewport.resized', size.cols, size.rows);
        setTimeout(() => this.resize(size.cols, size.rows));
        return terminal;
    }

    private emit(eventName: InstanceEvent, ...args: any[]) {
        this.eventEmitter.emit.apply(this.eventEmitter, [eventName, ...args]);
    }
}
