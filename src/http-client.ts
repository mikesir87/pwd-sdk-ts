

export interface RequestOptions {
    headers: { [name: string]: string };
}

export interface Response {
    status: number;
    body: any;
}


export class HttpClient {

    static get(url: string, options?: RequestOptions): Promise<Response> {
        return new Promise((resolve, reject) => {
            const xhr = this.createXhr('GET', url, options, resolve, reject);
            xhr.send();
        });
    }

    static post(url: string, body: string | Object, options?: RequestOptions): Promise<Response> {
        return new Promise((resolve, reject) => {
            const xhr = this.createXhr('POST', url, options, resolve, reject);
            if (body instanceof Object)
                body = JSON.stringify(body);
            xhr.send(body);
        });
    }

    static delete(url: string, options?: RequestOptions): Promise<Response> {
        return new Promise((resolve, reject) => {
            const xhr = this.createXhr('DELETE', url, options, resolve, reject);
            xhr.send();
        });
    }

    private static createXhr(method: string, url: string, options: RequestOptions, resolve: Function, reject: Function): XMLHttpRequest {
        const xhr = new XMLHttpRequest();

        xhr.open(method, url, true);
        xhr.withCredentials = true;
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        if (options && options.headers) {
            Object.entries(options.headers).forEach(([key, value]) => {
                xhr.setRequestHeader(key, value);
            });
        }

        xhr.onload = () => {
            const body = (xhr.getResponseHeader('Content-Type').toLowerCase() === 'application/json') ?
                JSON.parse(xhr.responseText) : xhr.responseText;
            const response = { status: xhr.status, body };

            if (xhr.status / 100 === 2) {
                resolve(response);
            }
            else {
                reject(response);
            }
        };

        return xhr;
    }
}