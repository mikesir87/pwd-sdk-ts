import { MaxInstancesCreated } from './errors';
import { HttpClient, Response } from './http-client';
import { InstanceDetails, InstanceEvents } from './instance';
import { Session, SessionOptions } from './session';
import { Util } from './util';

describe('Session', () => {
    const id = '75f16227-a960-4b2c-bb99-2d33f2807e85';
    const hostname = 'localhost';
    const options: SessionOptions = { instanceOptions: [] };
    const instanceIp = "192.168.3.4";
    const instanceName = "instance-1";

    let session: Session;

    let socket: any;
    let socketHandlers: { [eventName: string]: Function };
    let originalNewSocketIoConnection = Util.newSocketIoConnection;
    let overriddenNewSocketIoConnection: jasmine.Spy;

    let originalHttpClientPost = HttpClient.post;
    let overriddenHttpClientPost: jasmine.Spy;
    let originalHttpClientDelete = HttpClient.delete;
    let overriddenHttpClientDelete: jasmine.Spy;

    let instance: any;
    let instanceMetadata: InstanceDetails;
    let instanceHandlers: { [eventName: string]: Function };
    let originalNewInstance = Util.newInstance;
    let overriddenNewInstance: jasmine.Spy;


    beforeEach(() => {
        session = new Session(options);

        instance = {
            on: jasmine.createSpy('instance.on').and.callFake((event: string, handler: Function) => { instanceHandlers[event] = handler; }),
            destroy: jasmine.createSpy('instance.destory'),
            getName: jasmine.createSpy('instance.getName').and.returnValue(instanceName),
            write: jasmine.createSpy('instance.write'),
            resize: jasmine.createSpy('instance.resize'),
            setCpuMetrics: jasmine.createSpy('instance.setCpuMetrics'),
            setMemoryMetrics: jasmine.createSpy('instance.setMemoryMetrics'),
            setExposedPorts: jasmine.createSpy('instance.setExposedPorts'),
            getIp: jasmine.createSpy('instance.getIp').and.returnValue(instanceIp),
        };
        instanceHandlers = {};
        overriddenNewInstance = jasmine.createSpy('newInstance').and.callFake((metadata: InstanceDetails) => { instanceMetadata = metadata; return instance; });
        Util.newInstance = overriddenNewInstance;

        socket = {
            on: jasmine.createSpy('socket.on').and.callFake((event: string, handler: Function) => { socketHandlers[event] = handler; }),
            once: jasmine.createSpy('socket.once').and.callFake((event: string, handler: Function) => { socketHandlers[event] = handler; }),
            emit: jasmine.createSpy('socket.emit'),
        };
        socketHandlers = {};
        overriddenNewSocketIoConnection = jasmine.createSpy('newSocketIoConnection').and.returnValue(socket);
        Util.newSocketIoConnection = overriddenNewSocketIoConnection;

        overriddenHttpClientPost = jasmine.createSpy('httpClientPost').and.callFake(createInstanceSuccessCallback);
        HttpClient.post = overriddenHttpClientPost;
    });

    afterEach(() => {
        session.destroy();

        HttpClient.delete = originalHttpClientDelete;
        HttpClient.post = originalHttpClientPost;
        Util.newInstance = originalNewInstance;
        Util.newSocketIoConnection = originalNewSocketIoConnection;
    });


    it('initializes correctly', () => {
        session.initialize(id, hostname);

        expect(session.getId()).toBe(id);
        expect(session.getHostname()).toBe(hostname);
        expect(session.getInstances().length).toBe(0);
        expect(socket.on).toHaveBeenCalledWith('terminal out', jasmine.any(Function));
        expect(socket.on).toHaveBeenCalledWith('viewport resize', jasmine.any(Function));
        expect(socket.on).toHaveBeenCalledWith('instance stats', jasmine.any(Function));
    });

    it('publishes initialized event', () => {
        const initializationEventListener = jasmine.createSpy('initializedEventListener');
        session.on('initialized', initializationEventListener);

        session.initialize(id, hostname);

        expect(initializationEventListener).toHaveBeenCalledWith(session);
    });

    it('does not publish events when no longer listening', () => {
        const initializationEventListener = jasmine.createSpy('initializedEventListener');
        session.on('initialized', initializationEventListener);
        session.off('initialized', initializationEventListener);

        session.initialize(id, hostname);

        expect(initializationEventListener).not.toHaveBeenCalledWith(session);
    });

    it('creates instances correctly', (done) => {
        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(overriddenHttpClientPost).toHaveBeenCalled();
                expect(session.getInstances().length).toBe(1);

                expect(instanceHandlers['terminal.data']).toBeDefined();
                expect(instanceHandlers['terminal.viewport.resized']).toBeDefined();
                expect(instanceHandlers[InstanceEvents[0]]).toBeDefined();

                done();
            });
    });

    it('rebroadcasts instance events as namespaced session events', (done) => {
        const eventName = InstanceEvents[0];
        const eventListener = jasmine.createSpy('eventListener');
        session.on(`instance.${eventName}`, eventListener);

        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(instanceHandlers[eventName]).toBeDefined();
                
                instanceHandlers[eventName]("test");
                expect(eventListener).toHaveBeenCalledWith(instance, "test");

                done();
            });
    });

    it('relays data from instance terminals correctly', (done) => {
        const eventName = 'terminal.data';

        session.initialize(id, hostname);
        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(instanceHandlers[eventName]).toBeDefined();
                
                instanceHandlers[eventName]('test');
                expect(socket.emit).toHaveBeenCalledWith('terminal in', instanceName, 'test');

                done();
            });
    });

    it('sends terminal data on to instance terminals', (done) => {
        const data = "some data";
        session.initialize(id, hostname);
        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(socketHandlers['terminal out']).toBeDefined();

                socketHandlers['terminal out'](instanceName, data);
                expect(instance.write).toHaveBeenCalledWith(data);

                done();
            });
    });

    it('does not send terminal data to non-matching instance name', (done) => {
        const data = "some data";
        session.initialize(id, hostname);
        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(socketHandlers['terminal out']).toBeDefined();

                socketHandlers['terminal out']('another-instance-name', data);
                expect(instance.write).not.toHaveBeenCalledWith(data);

                done();
            });
    });

    it('sends viewport resize events from socket on to instances', (done) => {
        const cols = 60;
        const rows = 20;

        session.initialize(id, hostname);
        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(socketHandlers['viewport resize']).toBeDefined();

                socketHandlers['viewport resize'](cols, rows);
                expect(instance.resize).toHaveBeenCalledWith(cols, rows);

                done();
            });
    });

    it('sends viewport resize events from instance on to socket', (done) => {
        const cols = 60;
        const rows = 20;

        session.initialize(id, hostname);
        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(instanceHandlers['terminal.viewport.resized']).toBeDefined();

                instanceHandlers['terminal.viewport.resized'](instanceName, cols, rows);
                expect(socket.emit).toHaveBeenCalledWith('viewport resize', cols, rows);

                done();
            });
    });

    it('sends instance stat events from socket on to instances', (done) => {
        const cpuMetrics = 'cpu-metrics';
        const memoryMetrics = 'memory-metrics';
        const exposedPorts = [80];

        session.initialize(id, hostname);
        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(socketHandlers['instance stats']).toBeDefined();

                socketHandlers['instance stats'](instanceName, memoryMetrics, cpuMetrics, false, exposedPorts);
                expect(instance.setCpuMetrics).toHaveBeenCalledWith(cpuMetrics);
                expect(instance.setMemoryMetrics).toHaveBeenCalledWith(memoryMetrics);
                expect(instance.setExposedPorts).toHaveBeenCalledWith(exposedPorts);

                done();
            });
    });

    it('does not set exposed port when null', (done) => {
        session.initialize(id, hostname);
        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(socketHandlers['instance stats']).toBeDefined();

                socketHandlers['instance stats'](instanceName, 'm', 'c', false, null);
                expect(instance.setExposedPorts).not.toHaveBeenCalled();

                done();
            });
    });

    it('creates exposed port urls correctly', (done) => {
        session.initialize(id, hostname);
        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                expect(instanceMetadata).toBeDefined();
                expect(instanceMetadata.exposedPortUrlGenerator).toBeDefined();

                expect(instanceMetadata.exposedPortUrlGenerator(instance, 80))
                    .toBe(`pwd192_168_3_4-80.localhost`);

                done();
            });
    });

    it('throws MaxInstancesCreated when trying to create an instance when maxed out', (done) => {
        overriddenHttpClientPost = jasmine.createSpy('httpClientPost').and.callFake(createErrorResponse(409, null));
        HttpClient.post = overriddenHttpClientPost;

        session.createInstance({ terminalSelector: '.term' })
            .then(() => fail('Should not have used then'))
            .catch((err) => {
                expect(err instanceof MaxInstancesCreated).toBe(true);
                done();
            });
    });

    it('throws an error upon any other error during instance creation', (done) => {
        overriddenHttpClientPost = jasmine.createSpy('httpClientPost').and.callFake(createErrorResponse(400, { message: "Some message" }));
        HttpClient.post = overriddenHttpClientPost;

        session.createInstance({ terminalSelector: '.term' })
            .then(() => fail('Should not have used then'))
            .catch((err) => {
                expect(err instanceof Error).toBe(true);
                expect(err.message).toBe(JSON.stringify({message: "Some message"}));
                done();
            });
    });

    it('deletes instances correctly when providing instance', (done) => {
        overriddenHttpClientPost = jasmine.createSpy('httpClientPost').and.callFake(createInstanceSuccessCallback);
        HttpClient.post = overriddenHttpClientPost;
        overriddenHttpClientDelete = jasmine.createSpy('httpClientDelete').and.callFake(createInstanceSuccessCallback);
        HttpClient.delete = overriddenHttpClientDelete;

        const instanceRemoveEventListener = jasmine.createSpy('instance.removed');
        session.on('instance.removed', instanceRemoveEventListener);

        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                session.deleteInstance(instance)
                    .then(() => {
                        expect(session.getInstances().length).toBe(0);
                        expect(instanceRemoveEventListener).toHaveBeenCalledWith(instance);
                        done();
                    })
            });
    });

    it('deletes instances correctly when providing instance name', (done) => {
        overriddenHttpClientPost = jasmine.createSpy('httpClientPost').and.callFake(createInstanceSuccessCallback);
        HttpClient.post = overriddenHttpClientPost;
        overriddenHttpClientDelete = jasmine.createSpy('httpClientDelete').and.callFake(createInstanceSuccessCallback);
        HttpClient.delete = overriddenHttpClientDelete;

        const instanceRemoveEventListener = jasmine.createSpy('instance.removed');
        session.on('instance.removed', instanceRemoveEventListener);

        session.createInstance({ terminalSelector: '.term' })
            .then((instance) => {
                session.deleteInstance(instance.getName())
                    .then(() => {
                        expect(session.getInstances().length).toBe(0);
                        expect(instanceRemoveEventListener).toHaveBeenCalledWith(instance);
                        done();
                    })
            });
    });

    it('closes the session', (done) => {
        const sessionCloseListener = jasmine.createSpy('sessionCloseListener');
        session.on('session.closed', sessionCloseListener);
        session.initialize(id, hostname);

        socket.emit = jasmine.createSpy('custom.emit')
            .and.callFake((eventName: string) => {
                expect(eventName).toBe('session close');
                expect(socketHandlers['session end']).toBeDefined();
                socketHandlers['session end']();
            });

        session.close()
            .then(() => {
                expect(sessionCloseListener).toHaveBeenCalled();
                done();
            });
    });


    function createInstanceSuccessCallback(): Promise<Response> {
        return new Promise((accept, reject) => {
            accept({
                status: 200,
                body: { name: instanceName, ip: instanceIp, }
            });
        });
    }

    function createErrorResponse(status: number, body: any): () => Promise<Response> {
        return () => {
            return new Promise((accept, reject) => {
                reject({ status, body });
            });
        };
    }
});
