import { Session, SessionOptions } from './session';
import { HttpClient, Response } from './http-client';
import { Recaptcha } from './recaptcha';
import { InstanceOptions } from './instance';
import 'core-js/es6/promise';
import 'core-js/modules/es7.object.entries';

export interface PwdOptions {
    pwdHost?: string;
}

interface SessionRequestContext {
    session: Session;
    pwdOptions: PwdOptions;
    sessionOptions: SessionOptions;
}

export class PWD {

    static newSession(sessionOptions: SessionOptions, pwdOptions?: PwdOptions): Session {
        if (sessionOptions && sessionOptions.instanceOptions.length === 0) {
            console.warn('No terminal selectors specified.');
        }

        pwdOptions = pwdOptions || {};
        pwdOptions.pwdHost = pwdOptions.pwdHost || 'http://labs.play-with-docker.com';

        const session = new Session(sessionOptions);
        this.requestSession({ session, pwdOptions, sessionOptions });
        return session;
    }

    private static requestSession(context: SessionRequestContext, captchaCode?: string) {
        const requestOpts = { headers: {'Content-type': 'application/x-www-form-urlencoded'} };
        const data = 'session-duration=90m' + ((captchaCode === undefined) ?
                '' : `&g-recaptcha-response=${encodeURIComponent(captchaCode)}`);

        HttpClient.post(`${context.pwdOptions.pwdHost}/`, data, requestOpts)
            .then((response) => {
                context.session.initialize(response.body.session_id, response.body.hostname);
             })
            .catch((response: Response): Promise<Session> => {
                if (response.status === 403) {
                    if (context.sessionOptions.instanceOptions.length > 0) {
                        const terms = document.querySelectorAll(context.sessionOptions.instanceOptions[0].terminalSelector);
                        if (terms.length === 0) {
                            console.error('Unable to find a terminal to place recaptcha challenge');
                            return;
                        }

                        new Recaptcha().display(terms[0])
                            .then((captchaCode) => this.requestSession(context, captchaCode));
                    }
                }
            });
    }
}