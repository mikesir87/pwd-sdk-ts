import { ExposedPort } from './exposed-port';

describe('ExposedPort', () => {

  it('constructs correct', () => {
    const portNumber = 1234;
    const url = "http://test.localhost";

    const exposedPort = new ExposedPort(portNumber, url);
    
    expect(exposedPort.getPort()).toBe(portNumber);
    expect(exposedPort.getUrl()).toBe(url);
  });

});
