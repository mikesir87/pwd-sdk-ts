import { MaxInstancesCreated } from './errors';
import { Instance, InstanceOptions, InstanceEvents } from './instance';
import { HttpClient, Response } from './http-client';
import { Recaptcha } from './recaptcha';
import { Util } from './util';
import Emitter = require('tiny-emitter');
import * as io from 'socket.io-client';


export interface SessionOptions {
    imageName?: string;
    instanceOptions: InstanceOptions[];
}

export class Session {

    private id: string;
    private hostname: string;
    private eventEmitter: Emitter;
    private instances: Instance[];
    private socket: SocketIOClient.Socket;

    constructor(private options: SessionOptions) {
        this.instances = [];
        this.eventEmitter = new Emitter();
    }

    initialize(id: string, hostname: string) {
        this.id = id;
        this.hostname = hostname;
        this.eventEmitter.emit('initialized', this);

        this.socket = Util.newSocketIoConnection(this.hostname, {path: `/sessions/${id}/ws`});
        this.socket.on('terminal out', this.onTerminalOut.bind(this));
        this.socket.on('viewport resize', this.onViewportResize.bind(this));
        this.socket.on('instance stats', this.onInstanceStats.bind(this));

        this.options.instanceOptions.forEach(instance => {
            if (!Util.querySelector(instance.terminalSelector)) {
                return console.log(`Skipping instance with terminal selector: ${instance.terminalSelector} - unable to find DOM node`);
            }
            this.createInstance(instance);
        });
    }

    getHostname(): string {
        return this.hostname;
    }

    getId(): string {
        return this.id;
    }

    getInstances(): Instance[] {
        return this.instances;
    }

    createInstance(instanceOptions: InstanceOptions): Promise<Instance> {
        return HttpClient.post(
            `http://${this.hostname}/sessions/${this.id}/instances`,
            { ImageName: this.options.imageName },
            { headers: { 'Content-Type': 'application/json' }}
        ).then((response) => {
            const instanceMetadata = response.body;
            const instance = Util.newInstance({
                name: instanceMetadata.name,
                ip: instanceMetadata.ip,
                options: instanceOptions,
                exposedPortUrlGenerator: this.exposedPortUrlGenerator.bind(this),
            });

            // Listen to all events and rebroadcast through the session
            InstanceEvents.forEach(event => {
                instance.on(event, (...args: any[]) =>
                    this.eventEmitter.emit.apply(this.eventEmitter, [`instance.${event}`, instance, ...args]));
            });

            instance.on('terminal.data', (data: string) => this.onTerminalData(instance, data));
            instance.on('terminal.viewport.resized', this.onViewportResizeFromInstance.bind(this));

            this.addInstance(instance);
            return instance;
        }).catch((response: Response) => {
            if (response.status === 409) {
                throw new MaxInstancesCreated();
            }

            // Stringify, as the message will need co-erced into a string anyways
            throw new Error(JSON.stringify(response.body));
        });
    }

    deleteInstance(instance: Instance | string) {
        if (typeof instance === 'string') {
            instance = this.findInstance(instance);
        }
        
        return HttpClient.delete(`http://${this.hostname}/sessions/${this.id}/instances/${instance.getName()}`)
            .then(() => {
                this.instances = this.instances
                    .filter(i => i.getName() !== (<Instance>instance).getName());
                this.eventEmitter.emit('instance.removed', instance);
                return true;
            });
    }

    close(): Promise<boolean> {
        return new Promise((accept, reject) => {
            this.socket.once('session end', () => {
                this.eventEmitter.emit('session.closed', this);
                accept();
            });
            this.socket.emit('session close');
        });
    }

    on(eventName: string, callback: Function): Session {
        this.eventEmitter.on(eventName, callback);
        return this;
    }

    off(eventName: string, callback: Function): Session {
        this.eventEmitter.off(eventName, callback);
        return this;
    }

    destroy() {
        this.instances.forEach(instance => instance.destroy());
    }

    private addInstance(instance: Instance) {
        this.eventEmitter.emit('instance.new', instance);
        this.instances.push(instance);
    }

    private onTerminalData(instance: Instance, data: string) {
        this.socket.emit('terminal in', instance.getName(), data);
    }

    private onTerminalOut(name: string, data: string) {
        this.instances
            .filter(instance => instance.getName() === name)
            .forEach(instance => instance.write(data));
    }

    private onViewportResizeFromInstance(name: string, cols: number, rows: number) {
        this.socket.emit('viewport resize', cols, rows);
    }

    private onViewportResize(cols: number, rows: number) {
        this.instances.forEach(instance => instance.resize(cols, rows));
    }

    private onInstanceStats(name: string, memoryMetrics: string, cpuMetrics: string, isManager: boolean, exposedPorts: number[]) {
        const instance = this.findInstance(name);
        instance.setCpuMetrics(cpuMetrics);
        instance.setMemoryMetrics(memoryMetrics);

        if (exposedPorts != null)
            instance.setExposedPorts(exposedPorts);
    }

    private findInstance(name: string): Instance {
        const instances = this.instances.filter(i => i.getName() === name);
        if (instances.length !== 1)
            throw new Error(`Found non-one result set for instances with name ${name}`);
        return instances[0];
    }

    private exposedPortUrlGenerator(instance: Instance, port: number): string {
        return `pwd${instance.getIp().replace(/\./g, '_')}-${port}.${this.hostname}`;
    }
}