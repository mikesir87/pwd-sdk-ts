PWD SDK, Take Two
---

This SDK/library was developed as a thought experiment and as an alternative way to work with PWD (Play with Docker).

## Installation

```
npm install --save mikesir87-pwd-sdk
```



## Usage

The SDK can be used directly in the browser.  Simply add the script to your page.


## API

> This area is still being flushed out, but is enough to get you started.

The starting point is the **PWD** object.  From here, you can create a new **Session**.

### PWD

`PWD.newSession(sessionOptions: SessionOptions, pwdOptions?: PwdOptions) => Session`

Creates a new Session object. The new Session, however, may not be initialized yet (may require a ReCAPTCHA challenge to be completed).

#### Example

```javascript
const session = PWD.newSession({ instanceOptions: [ { terminalSelector: '.term1' } ] }, { pwdHost: 'localhost' });
```


### Session

The Session is the object that will let you create new instances, delete instances (eventually), and more.  You can also listen to events to hook in your own code.

#### API

- `getId() => string` - get the session identifier
- `getHostname() => string` - get the hostname for the session
- `getInstances() => Instance[]` - get all instances in the session
- `on(eventName, callback) => Session` - register an event listener (see below for events)
- `off(eventName, callback) => Session` - remove an event listener (see below for events)

#### Events

| Name          | Arguments      | Description                                                                                              |
|------------------|----------------|----------------------------------------------------------------------------------------------------------|
| initialized   | Session        | Invoked once the session has been created and has an id and remote reference (safe to add instances now) |


All Instance events (noted below) are also rebroadcasted through the Session, with a `instance.` prefix and the instance provided as the first argument.

```javascript
PWD.newSession({ instanceOptions: [ { terminalSelector: '.term1' } ] })
    .on('instance.port.new', function(instance, newPort) {
        console.log('Instance ' + instance.getName() + ' has a new port exposed: ' + newPort.getPort());
    });
```


### Instance

The Instance object represents a single instance/node in PWD.

#### API

- `getName() => string` - get the name of the instance
- `getIp() => string` - get the IP address for the instance (not a globally routed address)
- `getCpuMetrics() => string` - get the current CPU metrics for this instance
- `getMemoryMetrics() => string` - get the current memory metrics for this instance
- `getExposedPorts() => ExposedPort[]` - get the exposed ports for this instance
- `on(eventName, callback) => Instance` - register an event listener (see below for events)
- `off(eventName, callback) => Instance` - remove an event listener (see below for events)

#### Events

| Event Name       | Arguments              | Description                                                                                             |
|------------------|------------------------|---------------------------------------------------------------------------------------------------------|
| metrics.cpu      | string                 | Notified whenever the cpu metrics changes for the instance                                              |
| metrics.memory   | string                 | Notified whenever the memory metrics changes for the instance                                           |
| port.new         | ExposedPort            | Notified whenever a new port is exposed from the instance                                               |
| port.removed     | ExposedPort            | Notified whenever a port is no longer exposed from the instance                                         |
| terminal.data    | string                 | Notified upon every data entry made into the terminal                                                   |
| terminal.keydown | terminal, keyDownEvent | Attached as a "customKeydownHandler" to the xterm console. Allows for custom keyboard shortcut bindings |

As a reminder, ALL instance events are rebroadcasted at the Session level, where it's easier to hear events across all instances.


## Examples

### Adding a keyboard shortcut to clear the console

```javascript
PWD.newSession({ instanceOptions: [ { terminalSelector: '.term1' } ] })
  .on('instance.terminal.keydown', function(terminal, event) {
    if (event.metaKey && event.keyCode === 75)
      terminal.clear();
  });
``` 


## License

This is created solely as a proof of concept is available for anyone to use.
